from datetime import datetime
from pydantic import BaseModel


class _UserBase(BaseModel):
    email: str
    username: str | None = None


class UserCreate(_UserBase):
    hashed_password: str

    class Config:
        orm_mode = True


class User(_UserBase):
    id: int | None = None

    class Config:
        orm_mode = True


class _LeadBase(BaseModel):
    first_name: str
    last_name: str
    email: str
    company: str
    note: str


class LeadCreate(_LeadBase):
    pass


class Lead(_LeadBase):
    id: int
    owner_id: int
    create_at: datetime
    update_at: datetime

    class Config:
        orm_mode = True


class _ItemBase(BaseModel):  # serializer
    name: str
    description: str
    price: int
    on_offer: bool


class Item(_ItemBase):
    id: int

    class Config:
        orm_mode = True
