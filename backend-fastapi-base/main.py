from fastapi import FastAPI, Depends, HTTPException, security
from sqlalchemy.orm import Session
from typing import List

from schemas import UserCreate, User, LeadCreate, Lead
# from services import get_db, get_user_by_email, create_user
import services
app = FastAPI()


@app.post("/api/users")
async def create_user(user: UserCreate, db: Session = Depends(services.get_db)):
    db_user = await services.get_user_by_email(user.email, db)
    if db_user:
        raise HTTPException(status_code=400, detail="Email already in use")

    user = await services.create_user(user, db)

    return await services.create_token(user)


@app.post("/api/token")
async def generate_token(
    form_data: security.OAuth2PasswordRequestForm = Depends(),
    db: Session = Depends(services.get_db),
):
    user = await services.authenticate_user(form_data.username, form_data.password, db)

    if not user:
        raise HTTPException(status_code=401, detail="Invalid Credentials")

    return await services.create_token(user)


@app.get("/api/users/session", response_model=User)
async def get_user(user: User = Depends(services.get_current_user)):
    return user


@app.get("/api/leads", response_model=List[Lead])
async def get_leads(user: User = Depends(services.get_current_user), db: Session = Depends(services.get_db)):
    return await services.get_leads(user, db)


@app.get("/api/leads/{lead_id}", status_code=200)
async def get_lead(lead_id: int, user: User = Depends(services.get_current_user), db: Session = Depends(services.get_db)):
    return await services.get_lead(lead_id, user, db)


@app.post("/api/leads", response_model=User)
async def create_lead(lead: LeadCreate, user: User = Depends(services.get_current_user), db: Session = Depends(services.get_db)):
    return await services.create_lead(user, db, lead)


@app.put("/api/leads/{lead_id}", status_code=200)
async def update_lead(lead_id: int, lead: LeadCreate, user: User = Depends(services.get_current_user), db: Session = Depends(services.get_db)):
    await services.update_lead(lead_id, lead, user, db)
    return {"message", "Successfully Updated"}


@app.delete("/api/leads/{lead_id}", status_code=200)
async def delete_lead(lead_id: int, user: User = Depends(services.get_current_user), db: Session = Depends(services.get_db)):
    await services.delete_lead(lead_id, user, db)
    return {"message", "Successfully Deleted"}

@app.get("/api")
async def root():
    return {"message": "Awesome Leads Manager"}
