import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { createApp } from 'vue'
import router from './router'
import store from './store'
import App from './App.vue'

import './index.css'

library.add(fas, far, fab);

createApp(App).use(store).use(router).component('fa', FontAwesomeIcon).mount('#app')
